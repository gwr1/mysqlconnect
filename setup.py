#from setup3lib import setup
from setuptools import setup, find_packages
from os.path import join, dirname

setup(name='mysqlconnect',
      description='Python mysql engine wrapper',
      author='Andrey Pakosh',
      author_email='an.pakosh@gmail.com',
      url='https://gwr1@bitbucket.org/gwr1/mysqlconnect.git',
      packages=find_packages(),
      long_description=open(join(dirname(__file__), 'README.md')).read()
#      packages=['os', 'sqlalchemy'],
     )