import sqlalchemy
import os

os.environ['TDSVER'] = '8.0'

username = os.environ['MYSQL_ODBC_USERNAME']
password = os.environ['MYSQL_ODBC_PASSWORD']
server = host = os.environ['MYSQL_ODBC_HOST']
port = os.environ['MYSQL_ODBC_PORT']
hostport = '%s:%s' % (host, port)
database = os.environ['MYSQL_ODBC_DATABASE']
driver = os.environ['MYSQL_ODBC_DRIVER']

connection_string = "mysql+mysqlconnector://%s:%s@%s/%s" % (username, password, hostport, database)

engine = sqlalchemy.create_engine(connection_string)
#connection = engine.connect()
